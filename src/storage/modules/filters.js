/**
 * Mutations
 */
const SET_FILTER = 'SET_FILTER';

const state = {
    filters: {}
};

const mutations = {
    [SET_FILTER] (state, payload) {
        state.filters[Object.keys(payload)[0]] = payload[Object.keys(payload)[0]];
    }
};

const actions = {
    changeFilters (context, payload) {
        context.commit(SET_FILTER, payload);
    }
};

/**
 * Export
 */
export default {
    state,
    actions,
    mutations
}