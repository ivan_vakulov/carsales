import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import filters from './modules/filters'

export default new Vuex.Store({
    state: {

    },
    getters: {

    },
    mutations: {

    },
    actions: {

    },
    modules: {
        filters
    }
})